package com.cleveroad.bubbleanimation;

import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;

abstract class CircleDrawable {

    private Paint mPaint;
    private int mStartColor;
    private RectFloat mBounds;
    private boolean mDraw = true;
    private float mRadius;
    private Point mCenter;
    private int mAlpha;

    public CircleDrawable(Paint paint, int startColor) {
        mPaint = new Paint(paint);
        mStartColor = startColor;
        mRadius = 0;
        mCenter = new Point();
        mAlpha = 0;
    }

    void setStartColor(int startColor) {
        mStartColor = startColor;
    }

    public void onDraw(Canvas canvas) {
        if (mDraw) {
            mPaint.setColor(new Color(getColor()));
            canvas.drawCircle(mCenter.getPointX(), mCenter.getPointY(), mRadius, mPaint);
        }
    }

    public void drawLastFrame(Canvas canvas) {
        initLastFrameParams();
        mPaint.setColor(new Color(getColor()));
        canvas.drawCircle(mCenter.getPointX(), mCenter.getPointY(), mRadius, mPaint);
    }

    public RectFloat getBounds() {
        return mBounds;
    }

    public void setBounds(RectFloat bounds) {
        mBounds = bounds;
        initParams(mBounds);
    }

    boolean isDraw() {
        return mDraw;
    }

    void setDraw(boolean draw) {
        mDraw = draw;
    }

    Point getCenter() {
        return mCenter;
    }

    public void setCenter(Point point) {
        this.mCenter = point;
    }

    float getRadius() {
        return mRadius;
    }

    void setRadius(float radius) {
        mRadius = radius;
    }

    void setAlpha(int alpha) {
        mAlpha = alpha;
    }

    int getStartColor() {
        return mStartColor;
    }

    private int getColor() {
        int red = (getStartColor() & 0xff0000) >> 16;
        int green = (getStartColor() & 0x00ff00) >> 8;
        int blue = (getStartColor() & 0x0000ff);
        return Color.argb(mAlpha, red, green, blue);
    }

    abstract void initParams(RectFloat bounds);

    abstract void updateParams(float dt);

    abstract void initLastFrameParams();
}
