package com.cleveroad.bubbleanimation;


import ohos.agp.render.Paint;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;

class FourthCircle extends CircleDrawable {

    private static final float VISIBILITY_FRACTION_START = 52 * Constants.FRAME_SPEED;
    private static final float VISIBILITY_FRACTION_END = Constants.TOTAL_FRAMES * Constants.FRAME_SPEED;

    private static final float MOVE_FRACTION_START = VISIBILITY_FRACTION_START;
    private static final float MOVE_FRACTION_END = MOVE_FRACTION_START + 32 * Constants.FRAME_SPEED;

    private static final float MOVE_CENTER_FRACTION_START = MOVE_FRACTION_END;
    private static final float MOVE_CENTER_FRACTION_END = MOVE_CENTER_FRACTION_START + 8 * Constants.FRAME_SPEED;

    private float mLastMoveX;
    private float mMoveY;
    private float mRadius;

    private float mFirstMoveToCenterX;
    private float mLastMoveToCenterX;

    private float mFirstMoveToCenterY;
    private float mLastMoveToCenterY;

    public FourthCircle(Paint paint, int startColor) {
        super(paint, startColor);
    }

    @Override
    void initParams(RectFloat bounds) {
        mLastMoveX = bounds.left + bounds.getWidth() / 1.2f;
        mMoveY = bounds.getHeight() / 1.4f;
        mRadius = Math.max(bounds.getWidth(), bounds.getHeight()) / 7 * 4;

        mFirstMoveToCenterX = bounds.left + bounds.getWidth() / 1.2f;
        mLastMoveToCenterX = bounds.left + bounds.getWidth() / 1.1f;

        mFirstMoveToCenterY = bounds.getHeight() / 1.4f;
        mLastMoveToCenterY = bounds.getHeight() / 2;
    }

    @Override
    public void updateParams(float dt) {
        setDraw(DrawableUtils.between(dt, VISIBILITY_FRACTION_START, VISIBILITY_FRACTION_END));
        if (!isDraw()) {
            return;
        }
        if (DrawableUtils.between(dt, MOVE_FRACTION_START, MOVE_FRACTION_END)) {
            float t = DrawableUtils.normalize(dt, MOVE_FRACTION_START, MOVE_FRACTION_END);
            //getCenter().x = DrawableUtils.enlarge(getBounds().left, mLastMoveX, t);
            //getCenter().y = mMoveY;
            setCenter(new Point(DrawableUtils.enlarge(getBounds().left, mLastMoveX, t), mMoveY));
            setRadius(mRadius);
            setAlpha((int) DrawableUtils.enlarge(50, 255, t));
        } else if (DrawableUtils.between(dt, MOVE_CENTER_FRACTION_START, MOVE_CENTER_FRACTION_END)) {
            float t = DrawableUtils.normalize(dt, MOVE_CENTER_FRACTION_START, MOVE_CENTER_FRACTION_END);
            //getCenter().x = DrawableUtils.enlarge(mFirstMoveToCenterX, mLastMoveToCenterX, t);
            //getCenter().y = DrawableUtils.reduce(mFirstMoveToCenterY, mLastMoveToCenterY, t);
            setCenter(new Point(DrawableUtils.enlarge(mFirstMoveToCenterX, mLastMoveToCenterX, t), DrawableUtils.reduce(mFirstMoveToCenterY, mLastMoveToCenterY, t)));
        }
    }

    @Override
    void initLastFrameParams() {
        //getCenter().x = mLastMoveToCenterX;
        //getCenter().y = mLastMoveToCenterY;
        setCenter(new Point(mLastMoveToCenterX, mLastMoveToCenterY));
        setRadius(mRadius);
        setAlpha(255);
    }

}
