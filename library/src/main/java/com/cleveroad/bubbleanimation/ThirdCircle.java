package com.cleveroad.bubbleanimation;


import ohos.agp.render.Paint;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;

class ThirdCircle extends CircleDrawable {

    private static final float VISIBILITY_FRACTION_START = 48 * Constants.FRAME_SPEED;
    private static final float VISIBILITY_FRACTION_END = VISIBILITY_FRACTION_START + 24 * Constants.FRAME_SPEED;

    private float mLastX;

    private float mFirstY;
    private float mLastY;

    private float radius;

    public ThirdCircle(Paint paint, int startColor) {
        super(paint, startColor);
    }

    @Override
    void initParams(RectFloat bounds) {
        mLastX = bounds.left + bounds.getWidth();

        mFirstY = bounds.getHeight() / 1.8f;
        mLastY = bounds.getHeight() / 1.8f - bounds.getHeight() / 5;

        radius = bounds.getHeight() / 12;
    }

    @Override
    public void updateParams(float dt) {
        setDraw(DrawableUtils.between(dt, VISIBILITY_FRACTION_START, VISIBILITY_FRACTION_END));
        if (!isDraw()) {
            return;
        }
        float t = DrawableUtils.normalize(dt, VISIBILITY_FRACTION_START, VISIBILITY_FRACTION_END);
        //getCenter().x = DrawableUtils.enlarge(getBounds().left, mLastX, t);
        //getCenter().y = DrawableUtils.reduce(mFirstY, mLastY, t);
        setCenter(new Point(DrawableUtils.enlarge(getBounds().left, mLastX, t),DrawableUtils.reduce(mFirstY, mLastY, t)));
        setRadius(radius);
        setAlpha((int) DrawableUtils.enlarge(50, 255, t));
    }

    @Override
    void initLastFrameParams() {

    }

}
