package com.cleveroad.bubbleanimation;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * BubbleAnimationLayout can contains two children. One of them must has
 * {@link LayoutParams} with containerType = {@link LayoutParams#BASE_CONTAINER} - base view.
 * BubbleAnimationLayout uses this child for determining size. Another one child must has
 * {@link LayoutParams} with containerType = {@link LayoutParams#CONTEXT_CONTAINER} - context view.
 * Animation can be launched by swipe with start point x < indicatorWidth and end point x < getWidth / 10;
 */

public class BubbleAnimationLayout extends ComponentContainer implements Component.DrawTask {

    private static final int DEFAULT_INDICATOR_WIDTH = -1;

    private final Paint mIndicatorPaint = new Paint();
    private final RectFloat mIndicatorRectangle = new RectFloat();

    private int mAnimationColor = BubbleAnimationView.DEFAULT_ANIMATION_COLOR;
    private float mIndicatorWidth = DEFAULT_INDICATOR_WIDTH;

    private BubbleAnimationView mAnimationView;

    private Component mBaseContainer;

    private Component mContextView;

    private AnimatorGroup mForwardAnimatorSet;
    private AnimatorGroup mReverseAnimatorSet;
    private Animator mDefaultShowAnimator;

    private List<BubbleAnimationEndListener> mAnimationEndListeners;

    private float mDownPositionX = -1;

    public BubbleAnimationLayout(Context context) {
        this(context, null);
    }

    public BubbleAnimationLayout(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public BubbleAnimationLayout(Context context, AttrSet attrs, String styleName) {
        super(context, attrs, styleName);
        init(attrs);
    }

    private void init(AttrSet attrs) {
        mAnimationColor = BubbleAnimationView.DEFAULT_ANIMATION_COLOR;
        attrs.getAttr("bav_animation_color").ifPresent(attr -> mAnimationColor = attr.getColorValue().getValue());
        mIndicatorWidth = DEFAULT_INDICATOR_WIDTH;
        attrs.getAttr("bav_indicator_width").ifPresent(attr -> mIndicatorWidth = attr.getIntegerValue());

        mIndicatorPaint.setStyle(Paint.Style.FILL_STYLE);
        mIndicatorPaint.setColor(new Color(mAnimationColor));

        this.setBindStateChangedListener(new BindStateChangedListener() {
            @Override
            public void onComponentBoundToWindow(Component component) {

            }

            @Override
            public void onComponentUnboundFromWindow(Component component) {
                if (mForwardAnimatorSet != null && mForwardAnimatorSet.isRunning()) {
                    mForwardAnimatorSet.end();
                }
                if (mReverseAnimatorSet != null && mReverseAnimatorSet.isRunning()) {
                    mReverseAnimatorSet.end();
                }
            }
        });

        this.setTouchEventListener(new TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                if (mContextView != null && mContextView.getVisibility() == VISIBLE) {
                    return false;
                }
                int action = touchEvent.getAction();

                if (action == TouchEvent.PRIMARY_POINT_DOWN) {
                    if (touchEvent.getPointerPosition(touchEvent.getIndex()).getX() < getPaddingLeft() + mIndicatorWidth * 3) {
                        mDownPositionX = touchEvent.getPointerPosition(touchEvent.getIndex()).getX();
                        return true;
                    }
                } else if (action == TouchEvent.CANCEL || action == TouchEvent.PRIMARY_POINT_UP) {
                    if (mAnimationView != null && touchEvent.getPointerPosition(touchEvent.getIndex()).getX() - mDownPositionX > getEstimatedWidth() / 10 && mAnimationView.getVisibility() != VISIBLE) {
                        startViewAnimation(mDefaultShowAnimator);
                        return true;
                    }
                }
                return false;
            }
        });

        this.addDrawTask(this::onDraw);
    }

    @Override
    public void setLayoutConfig(LayoutConfig config) {
    }

    @Override
    public LayoutConfig createLayoutConfig(Context context, AttrSet attrSet) {
        return new LayoutParams(context, attrSet);
    }

    @Override
    public void estimateSize(int widthEstimatedConfig, int heightEstimatedConfig) {
        super.estimateSize(widthEstimatedConfig, heightEstimatedConfig);
        int height = EstimateSpec.getSize(heightEstimatedConfig);
        for (int i = 0; i < getChildCount(); i++) {
            Component child = getComponentAt(i);
            LayoutParams params = (LayoutParams) child.getLayoutConfig();
            if (params.getContainerType() == LayoutParams.BASE_CONTAINER) {

                measureChildWithMargins(child);
                height = child.getEstimatedHeight() + params.getMarginTop() + params.getMarginBottom();
                break;
            }
        }
        int heightSpec = EstimateSpec.getSizeWithMode(height, EstimateSpec.PRECISE);
        super.estimateSize(widthEstimatedConfig, heightSpec);

        for (int i = 0; i < getChildCount(); i++) {
            Component child = getComponentAt(i);
            if (child.getVisibility() == HIDE) {
                continue;
            }

            LayoutParams params = (LayoutParams) child.getLayoutConfig();
            int contentWidthSpec;
            if (params.getContainerType() == LayoutParams.BASE_CONTAINER) {
                contentWidthSpec = EstimateSpec.getSizeWithMode((int) (getEstimatedWidth() - params.getMarginLeft() - params.getMarginRight() - getPaddingLeft() - getPaddingRight() - mIndicatorWidth), EstimateSpec.PRECISE);
            } else {
                contentWidthSpec = EstimateSpec.getSizeWithMode(getEstimatedWidth() - params.getMarginLeft() - params.getMarginRight() - getPaddingLeft() - getPaddingRight(), EstimateSpec.PRECISE);
            }
            int contentHeightSpec = EstimateSpec.getSizeWithMode(getEstimatedHeight() - params.getMarginTop() - params.getMarginBottom() - getPaddingTop() - getPaddingBottom(), EstimateSpec.PRECISE);

            child.estimateSize(contentWidthSpec, contentHeightSpec);
        }
    }

    @Override
    public void arrange(int left, int top, int width, int height) {
        for (int i = 0; i < getChildCount(); i++) {
            Component child = getComponentAt(i);
            if (child.getVisibility() == HIDE) {
                continue;
            }
            LayoutParams params = (LayoutParams) child.getLayoutConfig();
            int l = getPaddingLeft() + params.getMarginLeft();
            int t = getPaddingTop() + params.getMarginTop();
            int r = getEstimatedWidth() - getPaddingRight() - params.getMarginRight();
            int b = getEstimatedHeight() - getPaddingBottom() - params.getMarginBottom();

            if (params.getContainerType() == LayoutParams.BASE_CONTAINER) {
                l += mIndicatorWidth;
            }
            child.arrange(l, t, r - l, b - t);
        }
    }

    @Override
    public void addComponent(Component childComponent, int index) {
        if (childComponent.getLayoutConfig() instanceof LayoutParams) {
            LayoutParams lp = (LayoutParams) childComponent.getLayoutConfig();
            if (lp.getContainerType() == LayoutParams.BASE_CONTAINER) {
                super.addComponent(childComponent, 0);
            } else {
                super.addComponent(childComponent, index);
            }
            checkChildrenCount();
            checkChildType(childComponent);
        }
    }

    @Override
    public void removeComponent(Component component) {
        super.removeComponent(component);
        nullingView(component);
    }

    @Override
    public void removeComponentAt(int index) {
        Component child = getComponentAt(index);
        nullingView(child);
        super.removeComponentAt(index);
    }


    @Override
    public void removeComponents(int start, int count) {
        for (int i = start; i < start + count && i < getChildCount(); i++) {
            Component child = getComponentAt(i);
            nullingView(child);
        }
        super.removeComponents(start, count);
    }

    @Override
    public void removeAllComponents() {
        super.removeAllComponents();
        nullingViews();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (mIndicatorWidth == DEFAULT_INDICATOR_WIDTH) {
            mIndicatorWidth = getEstimatedWidth() / 100;
        }
        mIndicatorRectangle.modify(getPaddingLeft(), getPaddingTop(), getPaddingLeft() + mIndicatorWidth, getEstimatedHeight() - getPaddingBottom());
        canvas.drawRect(mIndicatorRectangle, mIndicatorPaint);
    }

    /**
     * Setup animation color
     *
     * @param color animation color
     */
    @SuppressWarnings("unused")
    public void setAnimationColor(int color) {
        updateColor(color);
        getContext().getUITaskDispatcher().asyncDispatch(this::invalidate);
    }

    /**
     * Return animation color
     *
     * @return animation color
     */
    @SuppressWarnings("unused")
    public int getAnimationColor() {
        return mAnimationColor;
    }

    /**
     * Set indicator width
     *
     * @param indicatorWidth indicator width in px
     */
    @SuppressWarnings("unused")
    public void setIndicatorWidth(int indicatorWidth) {
        mIndicatorWidth = indicatorWidth;
        mIndicatorRectangle.modify(getPaddingLeft(), getPaddingTop(), getPaddingLeft() + mIndicatorWidth, getEstimatedHeight() - getPaddingBottom());
        postLayout();
    }

    /**
     * Return indicator width
     *
     * @return indicator width
     */
    @SuppressWarnings("unused")
    public float getIndicatorWidth() {
        return mIndicatorWidth;
    }

    /**
     * Reset view to initial state
     */
    @SuppressWarnings("unused")
    public void resetView() {
        if (mForwardAnimatorSet != null && mForwardAnimatorSet.isRunning()) {
            mForwardAnimatorSet.end();
        }
        if (mReverseAnimatorSet != null && mReverseAnimatorSet.isRunning()) {
            mReverseAnimatorSet.end();
        }
        if (mAnimationView != null && mAnimationView.isPlaying()) {
            mAnimationView.stopAnimation();
        }
        if (mBaseContainer != null) {
            mBaseContainer.setVisibility(VISIBLE);
        }
        if (mAnimationView != null) {
            mAnimationView.setAnimated(false);
        }
        if (mContextView != null) {
            //clear backgroud
            for (int i = 0; i < ((ComponentContainer) mContextView).getChildCount(); i++) {
                ((ComponentContainer) mContextView).getComponentAt(i).setBackground(null);
                ((ComponentContainer) mContextView).getComponentAt(i).setVisibility(VISIBLE);
                ((ComponentContainer) mContextView).getComponentAt(i).postLayout();
            }
            mContextView.setVisibility(HIDE);
        }
    }

    /**
     * Show context view with animation
     *
     * @param animator {@link Animator} for context view, if animator equals null, will launch default animation
     */
    @SuppressWarnings("unused")
    public void showContextViewWithAnimation(Animator animator) {
        startViewAnimation(animator);
    }

    /**
     * Show base view with animation
     *
     * @param animator Animator for base view, if animator equals null, will launch default animation
     */
    @SuppressWarnings("unused")
    public void showBaseViewWithAnimation(Animator animator) {
        reverseAnimation(animator);
    }

    /**
     * Show bubbles with animation
     *
     * @param listener {@link BubbleAnimationEndListener}
     */
    @SuppressWarnings("unused")
    public void showBubbleViewWithAnimation(BubbleAnimationEndListener listener) {
        startBubbleAnimation(listener);
    }

    /**
     * Hide bubbles with animation
     *
     * @param listener {@link BubbleAnimationEndListener}
     */
    public void hideBubbledViewWithAnimation(BubbleAnimationEndListener listener) {
        reverseBubbleAnimation(listener);
    }

    /**
     * Show bubbles
     */
    @SuppressWarnings("unused")
    public void showBubbledView() {
        if ((mForwardAnimatorSet != null && mForwardAnimatorSet.isRunning())
                || (mAnimationView != null && mAnimationView.isPlaying())
                || (mReverseAnimatorSet != null && mReverseAnimatorSet.isRunning())) {
            return;
        }
        if (mAnimationView != null) {
            mAnimationView.setAnimated(true);
        }
    }

    /**
     * Hide bubbles
     */
    @SuppressWarnings("unused")
    public void hideBubbledView() {
        if ((mForwardAnimatorSet != null && mForwardAnimatorSet.isRunning())
                || (mAnimationView != null && mAnimationView.isPlaying())
                || (mReverseAnimatorSet != null && mReverseAnimatorSet.isRunning())) {
            return;
        }
        if (mBaseContainer != null) {
            mBaseContainer.setVisibility(VISIBLE);
        }
        if (mAnimationView != null) {
            mAnimationView.setAnimated(false);
        }
    }

    /**
     * Show context view
     */
    @SuppressWarnings("unused")
    public void showContextView() {
        if ((mForwardAnimatorSet != null && mForwardAnimatorSet.isRunning())
                || (mAnimationView != null && mAnimationView.isPlaying())
                || (mReverseAnimatorSet != null && mReverseAnimatorSet.isRunning())) {
            return;
        }
        if (mBaseContainer != null) {
            mBaseContainer.setVisibility(HIDE);
        }
        if (mAnimationView != null) {
            mAnimationView.setAnimated(true);
        }
        if (mContextView != null) {
            mContextView.setVisibility(VISIBLE);
        }
    }

    /**
     * Show base view
     */
    @SuppressWarnings("unused")
    public void showBaseView() {
        if ((mForwardAnimatorSet != null && mForwardAnimatorSet.isRunning())
                || (mAnimationView != null && mAnimationView.isPlaying())
                || (mReverseAnimatorSet != null && mReverseAnimatorSet.isRunning())) {
            return;
        }
        if (mBaseContainer != null) {
            mBaseContainer.setVisibility(VISIBLE);
        }
        if (mAnimationView != null) {
            mAnimationView.setAnimated(false);
        }
        if (mContextView != null) {
            mContextView.setVisibility(HIDE);
        }
    }

    /**
     * Add {@link BubbleAnimationEndListener} which will call when show or hide animation ends
     *
     * @param animationEndListener {@link BubbleAnimationEndListener}
     */
    @SuppressWarnings("unused")
    public void addAnimationEndListener(BubbleAnimationEndListener animationEndListener) {
        if (mAnimationEndListeners == null) {
            mAnimationEndListeners = new ArrayList<>();
        }
        mAnimationEndListeners.add(animationEndListener);
    }

    /**
     * Remove {@link BubbleAnimationEndListener}
     *
     * @param animationEndListener {@link BubbleAnimationEndListener}
     */
    @SuppressWarnings("unused")
    public void removeAnimationEndListener(BubbleAnimationEndListener animationEndListener) {
        if (mAnimationEndListeners != null) {
            mAnimationEndListeners.remove(animationEndListener);
        }
    }

    /**
     * Remove all {@link BubbleAnimationEndListener}
     */
    @SuppressWarnings("unused")
    public void removeAllAnimationEndListeners() {
        if (mAnimationEndListeners != null) {
            mAnimationEndListeners.clear();
        }
    }

    /**
     * Set animator which will be called by touch event
     *
     * @param animator {@link Animator}
     */
    @SuppressWarnings("unused")
    public void setDefaultShowAnimator(Animator animator) {
        mDefaultShowAnimator = animator;
    }

    private void checkChildType(Component child) {
        LayoutParams params = (LayoutParams) child.getLayoutConfig();
        if (params.getContainerType() == LayoutParams.BASE_CONTAINER && mBaseContainer == null) {
            mBaseContainer = child;
        } else if (params.getContainerType() == LayoutParams.BASE_CONTAINER && mBaseContainer != null) {
            throw new IllegalArgumentException("Base container already defined");
        } else if (params.getContainerType() == LayoutParams.CONTEXT_CONTAINER && mContextView == null) {
            mContextView = child;
            child.setVisibility(HIDE);
        } else if (params.getContainerType() == LayoutParams.CONTEXT_CONTAINER && mContextView != null) {
            throw new IllegalArgumentException("Context container already defined");
        } else if (params.getContainerType() == LayoutParams.ANIM_CONTAINER && mAnimationView == null) {
            mAnimationView = (BubbleAnimationView) child;
            mAnimationView.setColor(mAnimationColor);
        } else if (params.getContainerType() == LayoutParams.ANIM_CONTAINER && mAnimationView != null) {
            throw new IllegalArgumentException("Anim container already defined");
        }
    }

    private void checkChildrenCount() {
        if (getChildCount() > 3) {
            throw new IllegalArgumentException("Number of children must be less than three");
        }
    }

    private void startViewAnimation(final Animator animator) {
        if (mForwardAnimatorSet == null) {
            mForwardAnimatorSet = new AnimatorGroup();
        } else if (mForwardAnimatorSet.isRunning()) {
            return;
        }
        if (mContextView == null || mAnimationView == null || mAnimationView.isAnimated()
                || mAnimationView.isPlaying()
                || (mReverseAnimatorSet != null && mReverseAnimatorSet.isRunning())) {
            return;
        }
        Animator contextAnimator = animator;
        if (contextAnimator == null) {
            contextAnimator = mContextView.createAnimatorProperty();
            ((AnimatorProperty) contextAnimator).alphaFrom(0.0f);
            ((AnimatorProperty) contextAnimator).alpha(1.0f);
            ((AnimatorProperty) contextAnimator).setDuration(500);
            //contextAnimator = ObjectAnimator.ofPropertyValuesHolder(mContextView, PropertyValuesHolder.ofFloat("alpha", 0.0f, 1.0f));
            //contextAnimator.setDuration(500);
        }
        ((AnimatorValue) mAnimationView.getAnimator()).setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                mAnimationView.setVisibility(VISIBLE);
            }

            @Override
            public void onStop(Animator animator) {
            }

            @Override
            public void onCancel(Animator animator) {
            }

            @Override
            public void onEnd(Animator animator) {
                //animator.removeListener(this);
                mAnimationView.setAnimatedWithNothing(true);

                if (mBaseContainer != null) {
                    mBaseContainer.setVisibility(HIDE);
                }
                if (mContextView != null) {
                    mContextView.setVisibility(VISIBLE);
                }
            }

            @Override
            public void onPause(Animator animator) {
            }

            @Override
            public void onResume(Animator animator) {
            }
        });

        mForwardAnimatorSet.runSerially(mAnimationView.getAnimator(), contextAnimator);
        mForwardAnimatorSet.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                mAnimationView.setVisibility(VISIBLE);
            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                mAnimationView.setAnimatedWithNothing(true);

                if (mAnimationEndListeners != null) {
                    for (BubbleAnimationEndListener listener : mAnimationEndListeners) {
                        listener.onEndAnimation(true, animator);
                    }
                }
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        mForwardAnimatorSet.start();
    }

    public void reverseAnimation(Animator animator) {
        if (mReverseAnimatorSet == null) {
            mReverseAnimatorSet = new AnimatorGroup();
        } else if (mReverseAnimatorSet.isRunning()) {
            return;
        }
        if (mContextView == null || mAnimationView == null || !mAnimationView.isAnimated()
                || mAnimationView.isPlaying()
                || mForwardAnimatorSet != null && mForwardAnimatorSet.isRunning()) {
            return;
        }
        Animator contextAnimator = animator;
        if (contextAnimator == null) {
            contextAnimator = mContextView.createAnimatorProperty();
            ((AnimatorProperty) contextAnimator).alphaFrom(1.0f);
            ((AnimatorProperty) contextAnimator).alpha(0.0f);
            ((AnimatorProperty) contextAnimator).setDuration(500);
        }
        ((AnimatorProperty) contextAnimator).setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                if (mBaseContainer != null) {
                    mBaseContainer.setVisibility(VISIBLE);
                }
                if (mContextView != null) {
                    mContextView.setVisibility(HIDE);
                }
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });

        mReverseAnimatorSet.runSerially(contextAnimator, mAnimationView.getReverseAnimator());
        mReverseAnimatorSet.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                if (mAnimationEndListeners != null) {
                    for (BubbleAnimationEndListener listener : mAnimationEndListeners) {
                        listener.onEndAnimation(false, animator);
                    }
                }
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });

        mReverseAnimatorSet.start();
    }

    private void startBubbleAnimation(final BubbleAnimationEndListener listener) {
        if (mAnimationView == null || mAnimationView.isAnimated()
                || mAnimationView.isPlaying()
                || (mForwardAnimatorSet != null && mForwardAnimatorSet.isRunning())
                || (mReverseAnimatorSet != null && mReverseAnimatorSet.isRunning())) {
            return;
        }
        ((AnimatorValue) mAnimationView.getAnimator()).setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                mAnimationView.setVisibility(VISIBLE);
            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                if (listener != null) {
                    listener.onEndAnimation(false, animator);
                }

                mAnimationView.setAnimatedWithNothing(true);
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });

        mAnimationView.getAnimator().start();
    }

    private void reverseBubbleAnimation(final BubbleAnimationEndListener listener) {
        if (mAnimationView == null || !mAnimationView.isAnimated()
                || mAnimationView.isPlaying()
                || (mForwardAnimatorSet != null && mForwardAnimatorSet.isRunning())
                || (mReverseAnimatorSet != null && mReverseAnimatorSet.isRunning())) {
            return;
        }
        if (mBaseContainer != null) {
            mBaseContainer.setVisibility(VISIBLE);
        }
        ((AnimatorValue) mAnimationView.getReverseAnimator()).setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                mAnimationView.setAnimatedWithNothing(false);
            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                mAnimationView.setAnimatedWithNothing(false);
                mAnimationView.setVisibility(INVISIBLE);

                if (listener != null) {
                    listener.onEndAnimation(false, animator);
                }
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });

        mAnimationView.getReverseAnimator().start();
    }

    private void nullingViews() {
        mBaseContainer = null;
        mContextView = null;
        mAnimationView = null;
    }

    private void nullingView(Component view) {
        if (view != null && view.getLayoutConfig() instanceof LayoutParams) {
            LayoutParams params = (LayoutParams) view.getLayoutConfig();
            if (params.getContainerType() == LayoutParams.BASE_CONTAINER) {
                mBaseContainer = null;
            } else if (params.getContainerType() == LayoutParams.CONTEXT_CONTAINER) {
                mContextView = null;
            }
        }
    }

    private void updateColor(int color) {
        mAnimationColor = color;
        RgbColor rgbColor = new RgbColor(color);
        mIndicatorPaint.setColor(new Color(Color.rgb(rgbColor.getRed(), rgbColor.getGreen(), rgbColor.getBlue())));
        if (mAnimationView != null) {
            mAnimationView.setAnimationColor(color);
        }
    }

    /**
     * Callback for determining end of animation
     */
    public interface BubbleAnimationEndListener {
        /**
         * Call when animation ends
         *
         * @param isForwardAnimation true if show, false if hide
         * @param animation          the animation which reached its end.
         */
        void onEndAnimation(boolean isForwardAnimation, Animator animation);
    }

    /**
     * Per-child layout information for layouts that support margins and view type.
     */
    public static class LayoutParams extends ComponentContainer.LayoutConfig {

        public static final int BASE_CONTAINER = 1;
        public static final int CONTEXT_CONTAINER = 0;
        private static final int NONE = 2;
        private static final int ANIM_CONTAINER = 3;

        private int mContainerType = NONE;

        public LayoutParams(Context c, AttrSet attrs) {
            super(c, attrs);
//            TypedArray typedArray = c.obtainStyledAttributes(attrs, R.styleable.BubbleAnimationView);
//            mContainerType = typedArray.getInt(R.styleable.BubbleAnimationView_bav_view_type, NONE);
//            typedArray.recycle();
            mContainerType = 2;
            attrs.getAttr("bav_view_type").ifPresent(attr -> mContainerType = attr.getIntegerValue());
        }

        public LayoutParams(int width, int height) {
            super(width, height);
        }

        public LayoutParams(ComponentContainer.LayoutConfig source) {
            super(source);
            if (source instanceof LayoutParams) {
                mContainerType = ((LayoutParams) source).getContainerType();
            }
        }

        public int getContainerType() {
            return mContainerType;
        }

        @SuppressWarnings("unused")
        public void setContainerType(int containerType) {
            mContainerType = containerType;
        }
    }

    protected void measureChildWithMargins(Component child) {

        final int childWidthMeasureSpec = child.getEstimatedWidth() + child.getPaddingLeft() + getMarginLeft() + getPaddingRight() + getMarginRight();
        final int childHeightMeasureSpec = child.getEstimatedHeight() + child.getPaddingTop() + getMarginTop() + getPaddingBottom() + getMarginBottom();

        child.estimateSize(childWidthMeasureSpec, childHeightMeasureSpec);
    }
}
