package com.cleveroad.bubbleanimation;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

import java.math.BigDecimal;

class BubbleAnimationView extends Component implements Component.DrawTask {

    public static final int DEFAULT_ANIMATION_COLOR = Color.RED.getValue();

    private AnimatorValue mForwardAnimator;
    private AnimatorValue mReverseValueAnimator;

    private RectFloat mBaseRectangle;

    private Paint mCirclePaint;

    private CircleDrawable[] mCircleDrawables;

    private boolean mAnimated = true;
    private int color = 0;

    public BubbleAnimationView(Context context) {
        this(context, null);
    }

    public BubbleAnimationView(Context context, AttrSet attrs) {
        this(context, attrs, 0);
    }

    public BubbleAnimationView(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void setColor(int color) {
        this.color = color;
        if (mCirclePaint == null) {
            mCirclePaint = new Paint();
        }
        mCirclePaint.setAntiAlias(true);
        mCirclePaint.setColor(new Color(color));

        mCircleDrawables = new CircleDrawable[6];
        CircleDrawable firstCircle = new FirstCircle(mCirclePaint, color);
        mCircleDrawables[0] = firstCircle;

        CircleDrawable secondCircle = new SecondCircle(mCirclePaint, color);
        mCircleDrawables[1] = secondCircle;

        CircleDrawable thirdCircle = new ThirdCircle(mCirclePaint, color);
        mCircleDrawables[2] = thirdCircle;

        CircleDrawable fourthCircle = new FourthCircle(mCirclePaint, color);
        mCircleDrawables[3] = fourthCircle;

        CircleDrawable fifthCircle = new FifthCircle(mCirclePaint, color);
        mCircleDrawables[4] = fifthCircle;

        CircleDrawable sixthCircle = new SixthCircle(mCirclePaint, color);
        mCircleDrawables[5] = sixthCircle;
    }

    private void init() {
        if (color == 0)
            color = DEFAULT_ANIMATION_COLOR;

        mCirclePaint = new Paint();
        mCirclePaint.setColor(new Color(color));
        mCirclePaint.setAntiAlias(true);

        mBaseRectangle = new RectFloat();

        mCircleDrawables = new CircleDrawable[6];
        CircleDrawable firstCircle = new FirstCircle(mCirclePaint, color);
        mCircleDrawables[0] = firstCircle;

        CircleDrawable secondCircle = new SecondCircle(mCirclePaint, color);
        mCircleDrawables[1] = secondCircle;

        CircleDrawable thirdCircle = new ThirdCircle(mCirclePaint, color);
        mCircleDrawables[2] = thirdCircle;

        CircleDrawable fourthCircle = new FourthCircle(mCirclePaint, color);
        mCircleDrawables[3] = fourthCircle;

        CircleDrawable fifthCircle = new FifthCircle(mCirclePaint, color);
        mCircleDrawables[4] = fifthCircle;

        CircleDrawable sixthCircle = new SixthCircle(mCirclePaint, color);
        mCircleDrawables[5] = sixthCircle;

        setAnimated(false);
        addDrawTask(this);
        initAnimator();
    }

    private void initAnimator() {
        mForwardAnimator = new AnimatorValue();
        mReverseValueAnimator = new AnimatorValue();

        mForwardAnimator.setCurveType(Animator.CurveType.LINEAR);
        mReverseValueAnimator.setCurveType(Animator.CurveType.LINEAR);

        long duration = (long) (Constants.TOTAL_DURATION + Constants.DEFAULT_SPEED_COEFFICIENT);
        mForwardAnimator.setDuration(duration);
        mReverseValueAnimator.setDuration(duration);

        AnimatorValue.ValueUpdateListener forwardUpdateListener =
                (AnimatorValue animatorValue, float v) -> {
                    updateParams(v);
                    invalidate();
                };
        AnimatorValue.ValueUpdateListener reverseUpdateListener =
                (AnimatorValue animatorValue, float v) -> {
                    updateParams(new BigDecimal(1).subtract(new BigDecimal(v)).floatValue());
                    invalidate();
                };
        mForwardAnimator.setValueUpdateListener(forwardUpdateListener);
        mReverseValueAnimator.setValueUpdateListener(reverseUpdateListener);

        mForwardAnimator.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                setVisibility(VISIBLE);
            }

            @Override
            public void onStop(Animator animator) {
            }

            @Override
            public void onCancel(Animator animator) {
            }

            @Override
            public void onEnd(Animator animator) {
                mAnimated = true;
            }

            @Override
            public void onPause(Animator animator) {
            }

            @Override
            public void onResume(Animator animator) {
            }
        });

        mReverseValueAnimator.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                mAnimated = false;
            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                mAnimated = false;
                setVisibility(INVISIBLE);
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
    }

    private void updateParams(float dt) {
        for (CircleDrawable circleDrawable : mCircleDrawables) {
            circleDrawable.updateParams(dt);
        }
    }

    @Override
    public void estimateSize(int widthEstimatedConfig, int heightEstimatedConfig) {
        super.estimateSize(widthEstimatedConfig, heightEstimatedConfig);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        mBaseRectangle.modify(getPaddingLeft(), getPaddingTop(), getEstimatedWidth() - getPaddingRight(), getEstimatedHeight() - getPaddingRight());
        for (CircleDrawable circleDrawable : mCircleDrawables) {
            circleDrawable.setBounds(mBaseRectangle);
        }
        if (mAnimated) {
            for (CircleDrawable circle : mCircleDrawables) {
                circle.drawLastFrame(canvas);
            }
        } else {
            for (CircleDrawable circle : mCircleDrawables) {
                circle.onDraw(canvas);
            }
        }
    }

    void setAnimationColor(int color) {
        mCirclePaint.setColor(new Color(color));
        for (CircleDrawable drawable : mCircleDrawables) {
            drawable.setStartColor(color);
        }
    }

    void setAnimatedWithNothing(boolean animated) {
        if (mAnimated != animated) {
            mAnimated = animated;
        }
    }

    void setAnimated(boolean animated) {
        if (mAnimated != animated) {
            mAnimated = animated;
            setVisibility(animated ? VISIBLE : INVISIBLE);
            getContext().getUITaskDispatcher().asyncDispatch(this::invalidate);
        }
    }

    boolean isAnimated() {
        return mAnimated;
    }

    boolean isPlaying() {
        return mForwardAnimator.isRunning()
                || mReverseValueAnimator.isRunning();
    }

    void stopAnimation() {
        if (mForwardAnimator.isRunning()) {
            mForwardAnimator.end();
        }
        if (mReverseValueAnimator.isRunning()) {
            mReverseValueAnimator.end();
        }
    }

    Animator getAnimator() {
        return mForwardAnimator;
    }

    Animator getReverseAnimator() {
        return mReverseValueAnimator;
    }


}
