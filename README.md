# BubbleAnimationLayout
## Menu
* [Project Introduction](#Project Introduction)
* [Function Description](#Function Description)
* [Function Display](#Function Display)
* [Integration Description](#Integration Description)
* [Instructions](#Instructions)
* [License](#License)


### Function Description
You don’t want your apps look and feel boring, do you? Add some bubbles! Bubble Animation Layout by Cleveroad is at your service. This component is extremely functional and suits all kinds of apps. It’s more than simple to get your app’s UI stand out and attract attention.It’s easy to add some spice and create something outstanding with Cleveroad Bubble Animation Layout library. Be sure, your app users will appreciate your efforts and imaginative approach.


### Function Display
1.dynamic picture

![overview](screenshot/ss_dynamic_1.gif)

2.static picture

![overview](screenshot/ss_static_mix.jpg)

### Integration Description
##### Method 1: Using har package directly
```
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
```

##### Method 2: gradle dependency
```
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'com.gitee.archermind-ti:bubbleanimation-layout:1.0.0-beta'
```

### Instructions
##### 1.Declare it in you layout file like this:
```
<com.cleveroad.bubbleanimation.BubbleAnimationLayout
        xmlns:ohos="http://schemas.huawei.com/res/ohos"
        xmlns:cmzl="http://schemas.huawei.com/apk/res/ohos"
        ohos:id="$+id:root"
        ohos:height="110vp"
        ohos:width="match_parent"
        ohos:bottom_padding="5vp"
        ohos:left_padding="10vp"
        ohos:right_padding="10vp"
        ohos:top_padding="5vp"
        cmzl:bav_animation_color="#666"
        cmzl:bav_indicator_width="20">

        <DirectionalLayout
            ohos:id="$+id:base_root"
            ohos:height="match_parent"
            ohos:width="match_parent"
            ohos:alignment="horizontal_center"
            ohos:background_element="#3000"
            ohos:orientation="horizontal"
            cmzl:bav_view_type="1">
        </DirectionalLayout>

        <com.cleveroad.bubbleanimation.BubbleAnimationView
            ohos:height="match_parent"
            ohos:width="match_parent"
            cmzl:bav_view_type="3">
        </com.cleveroad.bubbleanimation.BubbleAnimationView>

        <DirectionalLayout
            ohos:id="$+id:context_root"
            ohos:height="match_parent"
            ohos:width="match_parent"
            ohos:alignment="horizontal_center"
            ohos:orientation="horizontal"
            cmzl:bav_view_type="0">

            <Image
                ohos:id="$+id:image_1"
                ohos:height="50vp"
                ohos:width="50vp"
                ohos:image_src="$media:edit"
                ohos:layout_alignment="center"
                ohos:margin="15vp"
                ohos:padding="10vp"/>

            <Image
                ohos:id="$+id:image_2"
                ohos:height="50vp"
                ohos:width="50vp"
                ohos:image_src="$media:phone"
                ohos:layout_alignment="center"
                ohos:margin="15vp"
                ohos:padding="10vp"/>

            <Image
                ohos:id="$+id:image_3"
                ohos:height="50vp"
                ohos:width="50vp"
                ohos:image_src="$media:fav"
                ohos:layout_alignment="center"
                ohos:margin="15vp"/>

        </DirectionalLayout>

    </com.cleveroad.bubbleanimation.BubbleAnimationLayout>
```

##### 2.customization

###### Setup indicator and bubble animation color
You can change animation color with attribute 'app:bav_animation_color'
```xml
<com.cleveroad.bubbleanimation.BubbleAnimationLayout
    ...
    app:bav_animation_color="#b92714"
    >
```
or
```java
 BubbleAnimationLayout mBalBaseView = ...;
 mBalBaseView.setAnimationColor(Color.YELLOW);
```
###### Setup indicator width
You can change indicator width with attribute 'app:bav_indicator_width'
```xml
<com.cleveroad.bubbleanimation.BubbleAnimationLayout
    ...
    app:bav_indicator_width="10dp"
    >
```
or
```java
 BubbleAnimationLayout mBalBaseView = ...;
 mBalBaseView.setIndicatorWidth(30);
```
###### Show context container
You can show context container with animation (base container will be hidden)
```java
 BubbleAnimationLayout mBalBaseView = ...;
 View contextView = findViewById(R.id.fl_context_view);
 Animator animator = ObjectAnimator.ofPropertyValuesHolder(contextView, PropertyValuesHolder.ofFloat("alpha", 0.0f, 1.0f))
                    .setDuration(500);
 mBalBaseView.showContextViewWithAnimation(animator);
```
or without animation
```java
 BubbleAnimationLayout mBalBaseView = ...;
 mBalBaseView.showContextView();
```
###### Show base container
For displaying base container with animation call (context container and bubble view will be hidden)
```java
 BubbleAnimationLayout mBalBaseView = ...;
 View contextView = findViewById(R.id.fl_context_view);
 Animator animator = ObjectAnimator.ofPropertyValuesHolder(contextView, PropertyValuesHolder.ofFloat("alpha", 1.0f, 0.0f))
                    .setDuration(500);
 mBalBaseView.showBaseViewWithAnimation(animator);
```
or without animation
```java
 BubbleAnimationLayout mBalBaseView = ...;
 mBalBaseView.showBaseView();
```
###### Show/hide bubble view
You can show bubble view with animation (for handling ending of animation specify BubbleAnimationEndListener)
```java
 BubbleAnimationLayout mBalBaseView = ...;
 mBalBaseView.showBubbleViewWithAnimation(new BubbleAnimationLayout.BubbleAnimationEndListener() {
     @Override
     public void onEndAnimation(boolean isForwardAnimation, Animator animation) {
        //Do something
     }
 });
```

or without animation
```java
 BubbleAnimationLayout mBalBaseView = ...;
 mBalBaseView.showBubbledView();
```

For hiding with animation (for handling ending of animation specify BubbleAnimationEndListener) call
```java
 BubbleAnimationLayout mBalBaseView = ...;
 mBalBaseView.hideBubbledViewWithAnimation(new BubbleAnimationLayout.BubbleAnimationEndListener() {
     @Override
     public void onEndAnimation(boolean isForwardAnimation, Animator animation) {
        //Do something
     }
 });
```
or without animation
```java
 BubbleAnimationLayout mBalBaseView = ...;
 mBalBaseView.hideBubbledView();
```

###### Reset view
Call BubbleAnimationLayout#resetView() to reset view to initial state
```java
 BubbleAnimationLayout mBalBaseView = ...;
 mBalBaseView.resetView();
```
###### Handling ending of animation
For handling animation's ending declare BubbleAnimationEndListener
```java
 BubbleAnimationLayout mBalBaseView = ...;
 mBalBaseView.addAnimationEndListener(new BubbleAnimationLayout.BubbleAnimationEndListener() {
     @Override
     public void onEndAnimation(boolean isForwardAnimation, Animator animation) {
         //Do something
     }
 });
```

### License
```
MIT License

Copyright (c) 2016 Cleveroad Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
