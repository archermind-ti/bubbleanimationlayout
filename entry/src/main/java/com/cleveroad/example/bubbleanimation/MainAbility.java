package com.cleveroad.example.bubbleanimation;

import com.cleveroad.example.bubbleanimation.slice.MainAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.window.service.WindowManager;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
    }
}
