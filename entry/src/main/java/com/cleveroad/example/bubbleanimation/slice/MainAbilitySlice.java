package com.cleveroad.example.bubbleanimation.slice;

import com.cleveroad.example.bubbleanimation.DetailAbility;
import com.cleveroad.example.bubbleanimation.ResourceTable;
import com.cleveroad.example.bubbleanimation.bean.CustomBean;
import com.cleveroad.example.bubbleanimation.provider.CustomItemProvider;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.ListContainer;

import java.util.ArrayList;
import java.util.List;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        ListContainer listContainer = (ListContainer) findComponentById(ResourceTable.Id_lc);
        List<CustomBean> customBeans = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            CustomBean customBean = null;
            if (i % 4 == 0) {
                customBean = new CustomBean("Old man", "The Outline of Science written by J. Arthur Thomson. Original copyright 1922 by G. P. Putnam's Sons.", "10 Mar 2016", ResourceTable.Media_item_pic_1);
            } else if (i % 4 == 1) {
                customBean = new CustomBean("Old man", "The Outline of Science written by J. Arthur Thomson. Original copyright 1922 by G. P. Putnam's Sons.", "10 Mar 2016", ResourceTable.Media_item_pic_2);
            } else if (i % 4 == 2) {
                customBean = new CustomBean("Old man", "The Outline of Science written by J. Arthur Thomson. Original copyright 1922 by G. P. Putnam's Sons.", "10 Mar 2016", ResourceTable.Media_item_pic_3);
            } else
                customBean = new CustomBean("Old man", "The Outline of Science written by J. Arthur Thomson. Original copyright 1922 by G. P. Putnam's Sons.", "10 Mar 2016", ResourceTable.Media_item_pic_4);
            customBeans.add(customBean);
        }
        CustomItemProvider customItemProvider = new CustomItemProvider(getContext(), customBeans);
        customItemProvider.setProviderInfoListener(new CustomItemProvider.ProviderInfoListener() {
            @Override
            public void onGo2Detail(int avatar) {
                go2Detail(avatar);
            }

        });
        listContainer.setItemProvider(customItemProvider);
    }

    private void go2Detail(int avatar) {
        //有参数跳转
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(getBundleName())
                .withAbilityName(DetailAbility.class.getName())
                .build();

        // 把operation设置到intent中
        intent.setParam("avatar", avatar);
        intent.setOperation(operation);
        startAbility(intent);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
