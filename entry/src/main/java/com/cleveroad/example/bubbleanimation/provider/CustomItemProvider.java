/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cleveroad.example.bubbleanimation.provider;

import com.cleveroad.bubbleanimation.BubbleAnimationLayout;
import com.cleveroad.example.bubbleanimation.ResourceTable;
import com.cleveroad.example.bubbleanimation.bean.CustomBean;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.*;
import ohos.agp.components.element.PixelMapElement;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static ohos.agp.components.Component.INVISIBLE;
import static ohos.agp.components.Component.VISIBLE;

/**
 * CustomItemProvider
 **/
public class CustomItemProvider extends RecycleItemProvider {
    private List<CustomBean> dataList;
    private Context mContext;
    private ProviderInfoListener mListener;

    public CustomItemProvider(Context context, List<CustomBean> dataList) {
        this.mContext = context;
        this.dataList = dataList;
    }

    public void setProviderInfoListener(ProviderInfoListener listener) {
        this.mListener = listener;
    }

    @Override
    public int getCount() {
        return dataList == null || dataList.isEmpty() ? 0 : dataList.size();
    }

    @Override
    public Object getItem(int i) {
        return dataList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        ViewHolder viewHolder = null;
        if (component == null) {
            component = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_item_custom_provider, componentContainer, false);
            viewHolder = new ViewHolder((ComponentContainer) component);
            component.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) component.getTag();
        }
        ViewHolder finalViewHolder = viewHolder;

        viewHolder.tvName.setText(dataList.get(i).getName());
        viewHolder.tvDesc.setText(dataList.get(i).getDescription());
        viewHolder.tvDate.setText(dataList.get(i).getDate());
        viewHolder.ivAvatar.setImageAndDecodeBounds(dataList.get(i).getAvatarUrl());

        viewHolder.root.addAnimationEndListener((boolean isForwardAnimation, Animator animation) -> {
            finalViewHolder.image1.setVisibility(VISIBLE);
            finalViewHolder.image2.setVisibility(VISIBLE);
            finalViewHolder.image3.setVisibility(VISIBLE);
            finalViewHolder.image1.setAlpha(1.0f);
            finalViewHolder.image2.setAlpha(1.0f);
            finalViewHolder.image3.setAlpha(1.0f);
            finalViewHolder.image1.setRotation(0.0f);
            finalViewHolder.image2.setRotation(0.0f);
            finalViewHolder.image3.setRotation(0.0f);
        });

        viewHolder.image1.setClickedListener(com -> {
            finalViewHolder.image1.setBackground(new PixelMapElement(getPixelMapFromResource(ResourceTable.Media_red_circle)));
            finalViewHolder.image2.setVisibility(INVISIBLE);
            finalViewHolder.image3.setVisibility(INVISIBLE);
            finalViewHolder.root.hideBubbledViewWithAnimation(new BubbleAnimationLayout.BubbleAnimationEndListener() {
                @Override
                public void onEndAnimation(final boolean isForwardAnimation, Animator animation) {
                    AnimatorProperty animatorProperty = finalViewHolder.image1.createAnimatorProperty();
                    animatorProperty.rotate(360f).setDuration(500).setStateChangedListener(new Animator.StateChangedListener() {
                        @Override
                        public void onStart(Animator animator) {

                        }

                        @Override
                        public void onStop(Animator animator) {

                        }

                        @Override
                        public void onCancel(Animator animator) {

                        }

                        @Override
                        public void onEnd(Animator animator) {
//                            finalViewHolder.image1.setBackground(null);
//                            finalViewHolder.image2.setBackground(null);
//                            finalViewHolder.image3.setBackground(null);
//                            finalViewHolder.image1.setVisibility(INVISIBLE);
//                            finalViewHolder.image2.setVisibility(INVISIBLE);
//                            finalViewHolder.image3.setVisibility(INVISIBLE);
                            finalViewHolder.root.resetView();
                            if (mListener != null) {
                                mListener.onGo2Detail(dataList.get(i).getAvatarUrl());
                            }
                        }

                        @Override
                        public void onPause(Animator animator) {

                        }

                        @Override
                        public void onResume(Animator animator) {

                        }
                    }).start();
                }
            });
        });
        viewHolder.image2.setClickedListener(com -> {
            finalViewHolder.image2.setBackground(new PixelMapElement(getPixelMapFromResource(ResourceTable.Media_red_circle)));
            finalViewHolder.image1.setVisibility(INVISIBLE);
            finalViewHolder.image3.setVisibility(INVISIBLE);
            finalViewHolder.root.hideBubbledViewWithAnimation(new BubbleAnimationLayout.BubbleAnimationEndListener() {
                @Override
                public void onEndAnimation(final boolean isForwardAnimation, Animator animation) {
                    AnimatorProperty animatorProperty = finalViewHolder.image2.createAnimatorProperty();
                    animatorProperty.rotate(135f).setDuration(500).setStateChangedListener(new Animator.StateChangedListener() {
                        @Override
                        public void onStart(Animator animator) {

                        }

                        @Override
                        public void onStop(Animator animator) {

                        }

                        @Override
                        public void onCancel(Animator animator) {

                        }

                        @Override
                        public void onEnd(Animator animator) {
//                            finalViewHolder.image1.setBackground(null);
//                            finalViewHolder.image2.setBackground(null);
//                            finalViewHolder.image3.setBackground(null);
//                            finalViewHolder.image1.setVisibility(INVISIBLE);
//                            finalViewHolder.image2.setVisibility(INVISIBLE);
//                            finalViewHolder.image3.setVisibility(INVISIBLE);
                            finalViewHolder.root.resetView();
                            if (mListener != null) {
                                mListener.onGo2Detail(dataList.get(i).getAvatarUrl());
                            }
                        }

                        @Override
                        public void onPause(Animator animator) {

                        }

                        @Override
                        public void onResume(Animator animator) {

                        }
                    }).start();
                }
            });
        });
        viewHolder.image3.setClickedListener(com -> {
            finalViewHolder.image3.setBackground(new PixelMapElement(getPixelMapFromResource(ResourceTable.Media_red_circle)));
            finalViewHolder.image1.setVisibility(INVISIBLE);
            finalViewHolder.image2.setVisibility(INVISIBLE);
            finalViewHolder.root.hideBubbledViewWithAnimation(new BubbleAnimationLayout.BubbleAnimationEndListener() {
                @Override
                public void onEndAnimation(final boolean isForwardAnimation, Animator animation) {
                    AnimatorProperty animatorProperty = finalViewHolder.image3.createAnimatorProperty();
                    animatorProperty.rotate(360f).setDuration(500).setStateChangedListener(new Animator.StateChangedListener() {
                        @Override
                        public void onStart(Animator animator) {

                        }

                        @Override
                        public void onStop(Animator animator) {

                        }

                        @Override
                        public void onCancel(Animator animator) {

                        }

                        @Override
                        public void onEnd(Animator animator) {
//                            finalViewHolder.image1.setBackground(null);
//                            finalViewHolder.image2.setBackground(null);
//                            finalViewHolder.image3.setBackground(null);
//                            finalViewHolder.image1.setVisibility(INVISIBLE);
//                            finalViewHolder.image2.setVisibility(INVISIBLE);
//                            finalViewHolder.image3.setVisibility(INVISIBLE);
                            finalViewHolder.root.resetView();
                            if (mListener != null) {
                                mListener.onGo2Detail(dataList.get(i).getAvatarUrl());
                            }
                        }

                        @Override
                        public void onPause(Animator animator) {

                        }

                        @Override
                        public void onResume(Animator animator) {

                        }
                    }).start();
                }
            });
        });

        return component;
    }


    static class ViewHolder {
        BubbleAnimationLayout root;
        Text tvName;
        Text tvDesc;
        Text tvDate;
        Image ivAvatar;
        Image image1;
        Image image2;
        Image image3;

        public ViewHolder(ComponentContainer componentContainer) {
            root = (BubbleAnimationLayout) componentContainer.findComponentById(ResourceTable.Id_root);
            tvName = (Text) componentContainer.findComponentById(ResourceTable.Id_tv_name);
            tvDesc = (Text) componentContainer.findComponentById(ResourceTable.Id_tv_description);
            tvDate = (Text) componentContainer.findComponentById(ResourceTable.Id_tv_date);
            ivAvatar = (Image) componentContainer.findComponentById(ResourceTable.Id_image_avatar);
            image1 = (Image) componentContainer.findComponentById(ResourceTable.Id_image_1);
            image2 = (Image) componentContainer.findComponentById(ResourceTable.Id_image_2);
            image3 = (Image) componentContainer.findComponentById(ResourceTable.Id_image_3);
        }
    }

    private PixelMap getPixelMapFromResource(int resourceId) {
        InputStream inputStream = null;
        try {
            // 创建图像数据源ImageSource对象
            inputStream = mContext.getResourceManager().getResource(resourceId);
            ImageSource.SourceOptions srcOpts = new ImageSource.SourceOptions();
            srcOpts.formatHint = "image/jpg";
            ImageSource imageSource = ImageSource.create(inputStream, srcOpts);


            // 设置图片参数
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            return imageSource.createPixelmap(decodingOptions);
        } catch (IOException e) {

        } catch (NotExistException e) {

        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
        }
        return null;
    }

    public interface ProviderInfoListener {
        void onGo2Detail(int avatar);
    }
}
